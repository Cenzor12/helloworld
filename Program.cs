﻿//Подключаем модуль System, Содержит фундаментальные и базовые классы
using System;
//Создаем пространство имен HelloWorld
namespace HelloWorld
{
    class Program
    {
        //void Main точка входа приложения C#
        static void Main(string[] args)
        {
            //Выводит на экран консоли "Hello World!"
            Console.WriteLine("Hello World!");
            //Задерживаем приложение, чтобы прочитать выведенное
            Console.ReadLine();
        }
    }
}
